# Generated by Django 4.2.7 on 2023-11-25 18:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('back_end_accounts', '0003_alter_account_password'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='account',
            name='username',
        ),
    ]
