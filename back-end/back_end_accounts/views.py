from rest_framework import status
from django.conf import settings
from django.contrib.auth import login as django_login, logout
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken
from .models import Account
from .serializers import AccountSerializer, UserLoginSerializer
from rest_framework import viewsets
from rest_framework.decorators import (
    api_view,
    permission_classes,
    authentication_classes,
)

from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication


class AccountViewSet(viewsets.ModelViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            account = serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserLoginView(APIView):
    def post(self, request, *args, **kwargs):
        serializer = UserLoginSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.validated_data
            django_login(request, user)
            refresh = RefreshToken.for_user(user)  # Generate the refresh token

            response = Response(
                {
                    "account_id": user.id,
                    "first_name": user.first_name,
                    "account": str(user),
                },
                status=status.HTTP_200_OK,
            )

            # Ensure tokens are converted to string correctly
            response.set_cookie(
                "refresh_token",
                str(refresh),  # Convert RefreshToken object to string
                httponly=True,
                secure=False,  # Set to True if using HTTPS
                samesite="Lax",
            )
            response.set_cookie(
                "access_token",
                str(refresh.access_token),  # Convert AccessToken object to string
                httponly=True,
                secure=False,  # Set to True if using HTTPS
                samesite="Lax",
            )
            return response

        return Response(serializer.errors, status=status.HTTP_400_BAD_BAD_REQUEST)


@api_view(["POST"])
@permission_classes([AllowAny])
@authentication_classes([])
def logout_view(request):
    logout(request)
    response = Response(
        {"success": "Logged out successfully."}, status=status.HTTP_200_OK
    )
    response.delete_cookie("access_token")
    response.delete_cookie("refresh_token")
    response.delete_cookie("sessionid")
    response.delete_cookie("csrftoken")
    return response


@api_view(["GET"])
@permission_classes([IsAuthenticated])
def token_verify(request):
    return Response({"valid": True})


@api_view(["POST"])
def token_refresh(request):
    refresh_token = request.data.get("refresh")
    try:
        refresh = RefreshToken(refresh_token)
        response = Response(status=status.HTTP_200_OK)
        response.set_cookie(
            "access_token",
            str(refresh.access_token),
            httponly=True,
            secure=False,  # Set to True in production
            samesite="Lax",  # or 'Strict' based on your requirement
        )
        return response
    except Exception as e:
        return Response({"error": str(e)}, status=status.HTTP_401_UNAUTHORIZED)
