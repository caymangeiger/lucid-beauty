from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import (
    AccountViewSet,
    UserLoginView,
    logout_view,
    token_verify,
    token_refresh,
)

router = DefaultRouter()
router.register(r"accounts", AccountViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path("account/login/", UserLoginView.as_view(), name="account_login"),
    path("account/logout/", logout_view, name="account_logout"),
    path("token/verify/", token_verify, name="token_verify"),
    path("api/token/refresh/", token_refresh, name="token_refresh"),
]
