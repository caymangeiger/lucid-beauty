# Generated by Django 4.2.7 on 2023-11-05 06:42

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AdditionalService',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=500)),
                ('price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('min_duration', models.IntegerField(help_text='Minimum duration in minutes')),
                ('max_duration', models.IntegerField(help_text='Maximum duration in minutes')),
                ('category', models.CharField(max_length=50)),
                ('available', models.BooleanField(default=True)),
                ('discount', models.DecimalField(decimal_places=2, default=0, max_digits=10)),
                ('image', models.ImageField(blank=True, null=True, upload_to='services_images/')),
                ('slug', models.SlugField(max_length=255, unique=True)),
                ('meta_title', models.CharField(blank=True, max_length=100)),
                ('meta_description', models.TextField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='AgeRequirement',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('required', models.BooleanField(default=False)),
                ('age', models.IntegerField(help_text='Age requirement')),
                ('reason', models.CharField(blank=True, max_length=200)),
            ],
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=500)),
                ('price', models.DecimalField(decimal_places=2, max_digits=10)),
                ('min_duration', models.IntegerField(help_text='Minimum duration in minutes')),
                ('max_duration', models.IntegerField(help_text='Maximum duration in minutes')),
                ('category', models.CharField(max_length=50)),
                ('available', models.BooleanField(default=True)),
                ('discount', models.DecimalField(decimal_places=2, default=0, max_digits=10)),
                ('image', models.ImageField(blank=True, null=True, upload_to='services_images/')),
                ('slug', models.SlugField(max_length=255, unique=True)),
                ('meta_title', models.CharField(blank=True, max_length=100)),
                ('meta_description', models.TextField(blank=True)),
                ('age_requirement', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='back_end_services.agerequirement')),
            ],
        ),
        migrations.CreateModel(
            name='Appointment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('appointment_date', models.DateField()),
                ('appointment_time', models.TimeField()),
                ('account', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='appointments', to=settings.AUTH_USER_MODEL)),
                ('additional_services', models.ManyToManyField(blank=True, related_name='appointments', to='back_end_services.additionalservice')),
                ('service', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='appointment', to='back_end_services.service')),
            ],
        ),
        migrations.AddField(
            model_name='additionalservice',
            name='age_requirement',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='back_end_services.agerequirement'),
        ),
    ]
