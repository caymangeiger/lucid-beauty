from django.db import models
from back_end_accounts.models import Account
from django.db.models import Sum


class AgeRequirement(models.Model):
    required = models.BooleanField(default=False)
    age = models.IntegerField(help_text="Age requirement")
    reason = models.CharField(max_length=200, blank=True)


class Service(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=500)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    min_duration = models.IntegerField(help_text="Minimum duration in minutes")
    max_duration = models.IntegerField(help_text="Maximum duration in minutes")
    category = models.CharField(max_length=50)
    available = models.BooleanField(default=True)
    discount = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    age_requirement = models.ForeignKey(
        AgeRequirement, on_delete=models.CASCADE, null=True, blank=True
    )
    image = models.ImageField(upload_to="services_images/", blank=True, null=True)
    slug = models.SlugField(max_length=255, unique=True)
    meta_title = models.CharField(max_length=100, blank=True)
    meta_description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class AdditionalService(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=500)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    min_duration = models.IntegerField(help_text="Minimum duration in minutes")
    max_duration = models.IntegerField(help_text="Maximum duration in minutes")
    category = models.CharField(max_length=50)
    available = models.BooleanField(default=True)
    discount = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    age_requirement = models.ForeignKey(
        AgeRequirement, on_delete=models.CASCADE, null=True, blank=True
    )
    image = models.ImageField(upload_to="services_images/", blank=True, null=True)
    slug = models.SlugField(max_length=255, unique=True)
    meta_title = models.CharField(max_length=100, blank=True)
    meta_description = models.TextField(blank=True)

    def __str__(self):
        return self.name


class Appointment(models.Model):
    appointment_date = models.DateField()
    appointment_time = models.TimeField()
    guest_first_name = models.CharField(max_length=100, null=True, blank=True)
    guest_last_name = models.CharField(max_length=100, null=True, blank=True)
    guest_email = models.EmailField(null=True, blank=True)
    guest_phone_number = models.CharField(max_length=15, null=True, blank=True)
    account = models.ForeignKey(
        Account,
        on_delete=models.CASCADE,
        related_name="appointment_account",
        null=True,
        blank=True,
    )
    service = models.ForeignKey(
        Service,
        on_delete=models.CASCADE,
        related_name="appointment_service",
        null=True,
        unique=False,
    )
    additional_services = models.ManyToManyField(
        AdditionalService,
        related_name="appointment_additional_service",
        blank=True,
        unique=False
    )

    def __str__(self):
        return str(self.id)

    def calculate_cost(self):
        cost = 0

        if self.service:
            cost += self.service.price

        additional_services_cost = self.additional_services.aggregate(total=Sum("price"))['total'] or 0
        cost += additional_services_cost

        return cost

    @property
    def appointment_cost(self):
        return self.calculate_cost()
