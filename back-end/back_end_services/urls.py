from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ServiceViewSet, AdditionalServiceViewSet, AppointmentViewSet

router = DefaultRouter()
router.register(r"services", ServiceViewSet)
router.register(r"additionalservices", AdditionalServiceViewSet)
router.register(r"appointments", AppointmentViewSet, basename="appointment")


urlpatterns = [
    path("", include(router.urls)),
    path(
        "appointments/by-account/<int:account_id>/",
        AppointmentViewSet.as_view({"get": "by_account"}),
    ),
]
