from rest_framework import serializers
from .models import Service, AdditionalService, Appointment


class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = [
            "id",
            "name",
            "description",
            "price",
            "min_duration",
            "max_duration",
            "category",
            "available",
            "discount",
            "age_requirement",
            "image",
            "slug",
            "meta_title",
            "meta_description",
        ]

    def get_image_url(self, service):
        request = self.context.get("request")
        if service.image and hasattr(service.image, "url"):
            return request.build_absolute_uri(service.image.url)
        else:
            return None

    def to_representation(self, instance):
        representation = super(ServiceSerializer, self).to_representation(instance)
        if instance.image:
            representation["image"] = instance.image.url
        return representation


class AdditionalServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdditionalService
        fields = [
            "id",
            "name",
            "description",
            "price",
            "min_duration",
            "max_duration",
            "category",
            "available",
            "discount",
            "age_requirement",
            "image",
            "slug",
            "meta_title",
            "meta_description",
        ]

    def get_image_url(self, additional_services):
        request = self.context.get("request")
        if additional_services.image and hasattr(additional_services.image, "url"):
            return request.build_absolute_uri(additional_services.image.url)
        else:
            return None

    def to_representation(self, instance):
        representation = super(AdditionalServiceSerializer, self).to_representation(
            instance
        )
        if instance.image:
            representation["image"] = instance.image.url
        return representation


class AppointmentSerializer(serializers.ModelSerializer):
    appointment_cost = serializers.ReadOnlyField()
    guest_first_name = serializers.CharField(
        required=False, allow_null=True, max_length=100
    )
    guest_last_name = serializers.CharField(
        required=False, allow_null=True, max_length=100
    )
    guest_email = serializers.EmailField(required=False, allow_null=True)
    guest_phone_number = serializers.CharField(
        required=False, allow_null=True, max_length=15
    )

    class Meta:
        model = Appointment
        fields = [
            "id",
            "account",
            "appointment_date",
            "appointment_time",
            "appointment_cost",
            "service",
            "additional_services",
            "guest_first_name",
            "guest_last_name",
            "guest_email",
            "guest_phone_number",
        ]
        extra_kwargs = {
            "service": {"queryset": Service.objects.all()},
            "additional_services": {
                "queryset": AdditionalService.objects.all(),
                "many": True,
            },
        }

    def to_representation(self, instance):
        """Modify the representation of `service` and `additional_services`."""
        representation = super().to_representation(instance)
        service = instance.service
        additional_services = instance.additional_services.all()

        if service:
            representation["service"] = {
                "id": service.id,
                "name": service.name,
                "price": service.price,
                "image": service.image.url,
            }

        representation["additional_services"] = [
            {
                "id": additional_service.id,
                "name": additional_service.name,
                "price": additional_service.price,
                "image": additional_service.image.url,
                "quantity": instance.additional_services.filter(
                    id=additional_service.id
                ).count(),
            }
            for additional_service in additional_services
        ]

        return representation
