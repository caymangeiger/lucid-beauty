from django.contrib import admin
from .models import Service, AdditionalService, Appointment


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    pass


@admin.register(AdditionalService)
class AdditionalServiceAdmin(admin.ModelAdmin):
    pass


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    pass
