from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import (
    action,
    authentication_classes,
)
from .models import Service, AdditionalService, Appointment
from .serializers import (
    ServiceSerializer,
    AdditionalServiceSerializer,
    AppointmentSerializer,
)
from rest_framework import viewsets
from django.http import Http404


# YOU DONT NEED TO MAKE DETAIL JUST INCLUDE PK IN URL REST IS AUTOMATIC


@authentication_classes([])
class ServiceViewSet(viewsets.ModelViewSet):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            service = serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@authentication_classes([])
class AdditionalServiceViewSet(viewsets.ModelViewSet):
    queryset = AdditionalService.objects.all()
    serializer_class = AdditionalServiceSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            additional_service = serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@authentication_classes([])
class AppointmentViewSet(viewsets.ModelViewSet):
    serializer_class = AppointmentSerializer
    queryset = Appointment.objects.all()

    @action(detail=False, methods=["get"], url_path="by-account/(?P<account_id>\d+)")
    def by_account(self, request, account_id=None):
        """
        Returns appointments for a given account ID.
        """
        if account_id is not None:
            appointments = Appointment.objects.filter(account=account_id)
            serializer = self.get_serializer(appointments, many=True)
            return Response(serializer.data)
        else:
            return Response(
                {"error": "Account ID is required"}, status=status.HTTP_400_BAD_REQUEST
            )

    def create(self, request, *args, **kwargs):
        serializer = AppointmentSerializer(data=request.data)
        if serializer.is_valid():
            if "account" not in serializer.validated_data:
                appointment = serializer.save()
            else:
                appointment = serializer.save()

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        try:
            appointment = self.get_object()
            appointment.delete()
            return Response(
                {"detail": "Appointment deleted successfully."},
                status=status.HTTP_200_OK,
            )
        except Http404:
            return Response({"detail": "Not found."}, status=status.HTTP_404_NOT_FOUND)

    def partial_update(self, request, *args, **kwargs):
        kwargs["partial"] = True
        return self.update(request, *args, **kwargs)
